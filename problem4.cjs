const car_years = (inventory) => {
    let year_list = [];
    for (let index=0; index<inventory.length; index++) {
        year_list.push(inventory[index].car_year);
    }
    return year_list;
}

module.exports = car_years;