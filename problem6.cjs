const BMWandAudi = (inventory) => {
    let bmw_or_audi = [];
    for (let index=0; index<inventory.length; index++) {
        if (inventory[index].car_make == 'Audi' || inventory[index].car_make == 'BMW') {
            bmw_or_audi.push(inventory[index]);
        }
    }
    return bmw_or_audi;
}

module.exports = BMWandAudi;