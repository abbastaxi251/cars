const car_older_than_2000 = (inventory) => {
    let car_year_list = [];
    for (let index=0; index<inventory.length; index++) {
        if (inventory[index] < 2000) {
            car_year_list.push(inventory[index])
        }
    }
    return car_year_list;
}

module.exports = car_older_than_2000;