function swap(inventory,xp, yp)
{
    let temp = inventory[xp];
    inventory[xp] = inventory[yp];
    inventory[yp] = temp;
}
 
function alphabetical_sort(inventory)
{
    let min_idx;
 
    // One by one move boundary of unsorted subinventoryay
    for (let index = 0; index < inventory.length; index++)
    {
        // Find the minimum element in unsorted inventoryay
        min_idx = index;
        for (let index2 = index + 1; index2 < inventory.length; index2++)
        if (inventory[index2].car_model < inventory[min_idx].car_model)
            min_idx = index2;
 
        // Swap the found minimum element with the first element
        swap(inventory,min_idx, index);
    }
    return inventory;
}

module.exports = alphabetical_sort;