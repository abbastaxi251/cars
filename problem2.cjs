const latest_add = (inventory) => {
    let car_info = inventory[0]; 
    let latest_year = inventory[0].car_year;
    for (let index = 0; index<inventory.length; index++) {
        if (inventory[index].car_year >= latest_year) {
            latest_year = inventory[index].car_year;
            car_info = inventory[index]
        }
    }
    return car_info;
}

module.exports = latest_add;